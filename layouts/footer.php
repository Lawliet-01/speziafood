  <!-- Footer -->

 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 
<footer  class="page-footer hide-on-small-only" style="background:#37474f;padding-top: 10px; border-bottom-style: solid;border-bottom-width: 0px;">
      <div class="footer-copyright hide-on-small-only">
           <div class="container">
             <div class="left dev">
               <p class="white-text">Copyright &copy SpeziaFood |&nbsp<span class="blue-text">Developed by&nbsp&nbsp</span><i class="material-icons  waves-effect waves-light  white-text center" style="font-size:250% !important; color:#395382;">account_circle</i></p>
             </div>
             <div class="right row" style="display-flex;justify-content:space-between;">
                <a class="fa fa-facebook" href="https://www.facebook.com/speziafood"></a>
                <a class="fa fa-instagram" href="https://www.instagram.com/speziafood"></a>
              </div>
           </div>
       </div>
    </footer> 

    <footer class="page-footer hide-on-med-and-up" style="background:#37474f; border-bottom-style: solid;">
          <div class="container hide-on-med-and-up">
            <div class="col l6 s12 center">
                 <a class="fa fa-facebook" href="https://www.facebook.com/speziafood"></a>
                <a class="fa fa-instagram" href="https://www.instagram.com/speziafood"></a>
              </div>
          </div>
          <div class="footer-copyright">
            <div class="container center row dev">
            Copyright &copy SpeziaFood |<span class="blue-text" >Developed by&nbsp&nbsp</span>
            <br>
            <i class="material-icons center" style="font-size:200% !important; color:white;">account_circle</i> 
            </div>
          </div>
          
        </footer>
    <!-- Footer -->
    </body>
</html>
<script>

$(document).ready(function(){

  $(".button-collapse").sideNav();
  $('.parallax').parallax();

  $(".dev").click(function(){
    window.location ="http://widmits.com";
  });
});


</script>
