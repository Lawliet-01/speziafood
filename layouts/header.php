<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>SpeziaFood</title>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/materialize.css">
    <link rel="stylesheet" href="../fonts/icons/icon.woff2">
    <script type="text/javascript" src="../js/jquery_migrate.js"></script>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/materialize.js"></script>
    <!-- <script type="text/javascript" src="../js/custom/header.js"></script> -->
    <link rel="icon" href="../image/logo.png" type="image/x-icon"/>
    
</head>

<style>
  nav ul li a:hover{
    color:#f44336 !important;
  }

</style>

<body style="background:#eeeeee;">

    <header id="my-nav" class="navbar hoverable z-depth-3">
      <nav class="" style="background: #ffffff !important;">
        <a id="side" href="#" data-activates="mobile-demo" class="button-collapse sidenav-trigger black-text"><i class="material-icons">menu</i></a>
         <div class="nav-wrapper container">
          <!-- <a href="index.php" class="brand-logo black-text"><span>SPEZIAFOOD</span></a>         -->
          <a href="index.php" class="brand-logo hide-on-small-only"><img class="logoimg" src="../image/logo.png" alt=""></a>
          <a href="index.php" class="brand-logo hide-on-med-and-up"><img style="width:100px; height:50px !important;" src="../image/logo.png" alt=""></a>
          
          <ul id="nav-mobile" class="right hide-on-med-and-down">
             <li><a class="black-text" href="index.php#products"><span>Products</span></a></li>
             <li><a class="black-text" href="aboutus.php"><span>About Spezia</span></a></li>
             <li><a class="black-text" href="contactus.php"><span>Contact Us</span></a></li>
           </ul>
         </div>
       </nav>
    </header>
    <ul class="side-nav" id="mobile-demo">
       <li><a href="index.php" class="brand-logo"><h2 class="flow-text center" style="font-weight:500">SPEZIAFOOD</h2></a></li>
       <div class="divider"></div>
       <li><a href="index.php#products"><span>Products</span></a></li>
       <div class="divider"></div>
       <li><a href="aboutus.php"><span>About Spezia</span></a></li>
       <div class="divider"></div>
       <li><a href="contactus.php"><span>Contact Us</span></a></li>
       <div class="divider"></div>
     </ul> 
