$(document).ready(function(){

    $("#sub").click(function(){
        var name=$("#name").val();
        var phone=$("#phone").val();
        var email=$("#email").val();
        var msg=$("#msg").val();
        //alert(name+" "+phone+" "+email+" "+msg);
        $(this).prop("disabled",true);
        if(name!=""&&phone!=""&&email!=""&&msg!=""){
           if(email=="contact@speziafood.com"||email=="speziafood@gmail.com" || phone=="8287535578" || phone=="8010462180"){
                Materialize.toast('Hii Genius use your own Email and Phone no', 2000); 
                $(this).prop("disabled",false);
            } 
           else{
            if(validateEmail(email)&&validatePhone(phone))
                {
                $.post("../api/functions/sendMail.php",{
                    name:name,
                    email:email,
                    phone:phone,
                    msg:msg
                },function(data){
                    $("#sub").prop("disabled",false);
                //    alert(data);
                    if(data=="success"){
                        Materialize.toast('Your Message is sent', 2000);
                    }
                    else if(data=="notsent")
                    {
                        Materialize.toast('Please ensure Your Email is correct', 2000);
                    }
                    else if(data=="error")
                    {
                        Materialize.toast('Your Message is not sent try again', 2000);
                    }
                    else
                    {
                        Materialize.toast('Something Went Wrong Please try after sometime', 2000);
                    }
                });
            }
            else
            {
                Materialize.toast('Please enter a valid Email and Phone-No', 2000);
                $(this).prop("disabled",false);
            }   
         }
        }
        else{
            Materialize.toast('Please enter all details', 2000);
            $(this).prop("disabled",false);
         }
    
    
    });




    // -----------------------------Functions--------------------------------------
      //Email validate
      function validateEmail(sEmail)
      {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)){
          return true;
        }
        else
        {
            return false;
        }
      }

      function validatePhone(sno)
      {
        var filter = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$/;
        if (filter.test(sno)){
          return true;
        }
        else
        {
            return false;
        }
      }
});
