
$.getJSON("../json/products.json", function(data){
    if(id<=2){
        showContent(data[id]);
    }
    else{
        window.location.href ="index.php";
    }
});

function showContent(product)
{
    var img_1 = "../image/product/"+product['images'][0];
    var img_2 = "../image/product/"+product['images'][1];
    $("#content").append("<div class='row'>"+
        "<div class='col l3 m3 s12'>"+
            "<img class='my-pro responsive-img product' src='"+img_1+"' style='' alt='Image'>"+
            "<div class='row' style='margin-top:20px;'>"+
                "<div class='col m4 s4 l4'>"+
                    "<img class='my-pro responsive-img product_s' src='"+img_1+"' alt='Image' style=''>"+
                "</div>"+
                "<div class='col m4 s4 l4'>"+
                    "<img  class='my-pro responsive-img product_s' src='"+img_2+"' alt='Image' style=''>"+
                "</div>"+
            "</div>"+
        "</div>"+
        "<div class='col l6 m6 s12 offset-l1'>"+
            "<h3 class='font_k'>"+product['name']+"</h3>"+
            "<p style=''>"+product['desc']+"</p>"+
            "<span>Price: "+product['unit'][0]+" Rs"+product['price'][0]+"/-"+"</span><br>"+
            "<span>&nbsp &nbsp &nbsp &nbsp &nbsp"+product['unit'][1]+" Rs"+product['price'][1]+"/-"+"</span>"+
        "</div>"+    
    "</div>");    
    
    change_img();
}

function change_img(){
    $('.product_s').click(function(){
       var i_src = $(this).attr('src');
       // alert(i_src);
        $('.product').attr('src', i_src);
    });
}
