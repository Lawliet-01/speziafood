$(document).ready(function(){
    $(".button-collapse").sideNav();
    $('.parallax').parallax();
    
     window.sr = ScrollReveal({ reset: true });

     sr.reveal('.foo-2', { 
      origin: 'right', 
      duration: 2000 
      });

      sr.reveal('.foo-1', {origin: 'left', duration: 3000 });
  });