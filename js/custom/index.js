$(document).ready(function(){

    $(".button-collapse").sideNav();
    // $('.parallax').parallax();
    
    $('.carousel.carousel-slider').carousel({
        fullWidth: true,
        indicators: true
      });
     
      setInterval(function() {
        $('.carousel').carousel('next');
      }, 3000);


      $(".prodiv").click(function(){
        proid=$(this).data("value");
        window.location.href="showcaseproducts.php?proid="+proid;
      });


      window.sr = ScrollReveal({ reset: true });

      sr.reveal('.foo-2', { 
       origin: 'right', 
       duration: 2000 
       });
 
       sr.reveal('.foo-1', {origin: 'left', duration: 3000 });  
       
       sr.reveal('.foo-4', { 
        viewFactor: 0.5
      });

});
