  <!-- Header -->
  <?php include("../layouts/header.php"); ?>
  <!-- Header -->
  
  <style media="screen">
    nav {
          background:#0b8798;
        }

      .img_mail{
           height: auto;
           -webkit-border-radius: 50%;
           -moz-border-radius: 50%;
           -ms-border-radius: 50%;
           -o-border-radius: 50%;
           border-radius: 50%;
       }

    .avatar {
       vertical-align: middle;
       width: 70px;
       height: 70px;
       border-radius: 70%;
     }

   

    .contactbg{
      background-image:url('../image/contact-us.jpg');
      background-size:cover;
      /* background-repeat:no-repeat; */
      background-position: center center;

      /* height:100% */
    }
  </style>

     <!-- Main Content -->
     
     <section style="">
     <div class="row">
       <div class="col l6 m6 s12 center">
         <img class="img_mail responsive-img hoverable" style="width:50%; margin-top:20px;"  src="../image/email-2.gif" alt="Contact gif">
       </div>
       <div class="col l6 m6 s12">
         <div class="" >
           <h2 style="font-weight:300">Contact us</h2>
           <h5 class="flow-text" style="color: #494949; line-height:150%;">Share your thoughts with us. Give us suggestion to improve or become a distributer of Spezia, we will reply back to you at the earliest.</h5>
         </div>
       </div>
     </div>

     <!-- <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> -->

     <div class="row teal darken-1" style="margin-bottom:0px;">
       <div class="container">
         <div class="col l6 m6 s12" style="margin: 25px 0px 25px 0px;">
           <div class="col l6 m6 s6 offset-l4 offset-m4 offset-s4">
             <img class="avatar white"  src="../image/store.png" alt="storepng">
           </div>
           <div class="col l12 m12 s12 center">
             <h5>Bring Spezia Closer</h5>
             <span class="">Become a Spezia Distributer</span>
            </div>
         </div>

         <div class="col l6 m6 s12" style="margin: 25px 0px 25px 0px;">
           <div class="col l6 m6 s6 offset-l4 offset-m4 offset-s4">
             <img class="avatar white responsive-img"  src="../image/suggestion.png" alt="suggestion png">
           </div>
           <div class="col l12 m12 s12 center">
             <h5>Provide Feedback</h5>
             <span class="">We always welcome your feedback</span>
           </div>
         </div>
       </div>
     </div>

     <!-- <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> -->

     <div  class="contactbg" style="">
       <div class="row container" style="padding-top:20px; margin-bottom:-20px;">
           <div class="col l6 m6 s12">
              <div class="row">
                  <div class="col l2 s2 m2" style="margin-top:24px; padding-right:50px;">
                    <i class="medium material-icons teal-text">place</i>
                  </div>
                  <div class="col l9 m9 s9">
                    <h6 style="font-weight:400">Address</h6>
                    <span>228/10, Part-2, Mukundpur, New Delhi, India 110042</span>
                  </div>
              </div>

              <div class="row">
                  <div class="col l2 s2 m2" style="margin-top:24px; padding-right:50px;">
                    <i class="medium material-icons teal-text">phone</i>
                  </div>
                  <div class="col l9 m9 s9">
                    <h6 style="font-weight:400">Phone No.</h6>
                    <span>+91 8287535578</span><br>
                    <span>+91 8010462180</span>

                  </div>
              </div>

              <div class="row">
                  <div class="col l2 s2 m2" style="margin-top:24px; padding-right:50px;">
                    <i class="medium material-icons teal-text">mail</i>
                  </div>
                  <div class="col l9 m9 s9">
                    <h6 style="font-weight:400">Email</h6>
                    <span>speziafood@gmail.com</span>
                  </div>
              </div>
           </div>
           <div id="contact" class="col l6 m6 s12 z-depth-4 card-panel white">
             <form class="" action="index.html" method="post">
               <div class="input-field">
                 <input id="name" type="text" name="" value="">
                 <label for="name">Name</label>
               </div>
               <div class="input-field">
                 <input id="phone" type="text" name="" value="">
                 <label for="phone">Phone No</label>
               </div>
               <div class="input-field">
                 <input id="email" type="email" name="" value="">
                 <label for="email">Email</label>
               </div>
               <div class="input-field">
                 <textarea id="msg" class="materialize-textarea" value=""></textarea>
                 <label for="msg">Message</label>
               </div>
               <div class="input-field" style="padding-bottom:10px;">
                 <button id="sub" class="btn" type="button" name="button">Send Message</button>
               </div>
             </form>
           </div>
         </div>
     </div>

     <!-- <><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><> -->
    </section>
     <!-- Main Content -->


    <!-- Footer -->
    <?php include("../layouts/footer.php"); ?>
    <!-- Footer -->

<script type="text/javascript" src="../js/custom/contact.js"></script>
