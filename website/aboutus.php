<!-- header -->
  <?php include("../layouts/header.php");  ?>
  <link href="https://fonts.googleapis.com/css?family=Dancing+Script|Kavivanar|Rajdhani|Montserrat" rel="stylesheet">
  <script src="../js/scrollreveal.js"></script>
<!-- header -->
<style media="screen">

#center{
  position: absolute;
  margin-left: 50px;
  margin-top: 150px;
  margin-right: 50px;
  margin-bottom:20px;

  border: solid;
  padding-top: 20px;
  padding-bottom:20px;
  padding-left: 10px;
  padding-right: 10px;
  border-left-width: 10px;
  border-right-width: 10px;
  border-top-width: 10px;
  border-bottom-width: 10px;

}

 @media only screen
      and (min-device-width : 320px)
      and (max-device-width : 480px) {
        #center{
             margin-top:50px !important;
          }

          .carousel .carousel-slider{
            height:200px;
          }
        }

.avatar {
       vertical-align: middle;
       width: 70px;
       height: 70px;
       border-radius: 70%;
     }

.content{
  padding-top: 20px;
  font-size: 125%;
}


</style>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/animate.css@3.5.2/animate.min.css">
    <section style=" ">
      <div class="parallax-container">
       <div class="parallax"><img class="responsive-img" src="../image/about.jpg"></div>
       <div id="center" class="white-text animated bounceInDown">
         <span style="font-family:'Dekko'; font-size:6vh;" class="flow-text hoverable foo-1">" WHERE TASTE MEETS THE MYTHS "</span>
       </div>
     </div>

  <!-- section1 starts -->
    <div class="" style="background:#f7f7f7; margin-top:-20px; margin-bottom:-20px;">
      <div class="container">
        <div class="center " style="padding-top:1px;">
          <h2 class="foo-2" style="font-weight:300; font-family: 'Dancing Script', cursive; color:#4b5d85;">About Us</h2>
        </div>
        <p>
          <span style="font-family:'Montserrat', sans-serif !important; font-size:150%" class="flow-text foo-1">At Spezia, we are passionate about providing organic spices, Seasonings and organic cooking ingredients packed with flavour, aroma and provenance.</span>
          <br>
          <br>
          <span style="font-family:'Montserrat', sans-serif !important; font-size:120%" class="flow-text foo-2">
            We want to make you smile by providing you with the organic spices you want to buy without having to compromise.
            <br>
            <br>
            We believe, When you have good ingredients, cooking doesn't require a lot of instruction because you can never go very wrong.
          </span>
        </p>
      </div>
    </div>


    <div class="parallax-container row">
       <div class="parallax" style="opacity:0.3"><img class="responsive-img" src="../image/vision.jpg"></div>
        <div class="container">
          <h2 class="center foo-1" style="font-weight:400; font-family: 'Dancing Script', cursive; color:#252640; margin-top:10vh;">Our Vision</h2>
          <h2 class="flow-text center foo-2" style="font-weight:300; font-size:200%; padding-bottom:10px;">We aims to be an indispensable food ingredients solution by upholding its impeccable standards in quality, sustainability and consistency. Spezia strives to provide valuable service to our customers</h2>
       </div>
    </div>
  <!-- section3 ends -->
    <div class="row center" style="background:#f44336; display:flex; justify-content:center !important; margin-top:-20px;margin-bottom: -10px;">
        <h2 class="flow-text white-text" style=" font-family: 'Rajdhani', sans-serif !important; " >
          Are you a stockist looking to sell our delicious spices?
          <span class="" style="font-weight:bold;"><a href="contactus.php#contact" style="color:#e6ee9c;">Get in touch</a></span>
        </h2>
    </div>
 </section>



    <!-- Footer -->
      <?php include("../layouts/footer.php"); ?>
    <!-- Footer -->

<script type="text/javascript" src="../js/custom/aboutus.js"></script>
