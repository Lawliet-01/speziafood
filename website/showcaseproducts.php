<?php include("../layouts/header.php"); ?>
<?php
    if(isset($_GET['proid']))
    {
        $proid = $_GET['proid'];
        echo "<script>var id=".$proid."</script>";
    }
?>
<link href="https://fonts.googleapis.com/css?family=Dancing+Script|Kavivanar|Rajdhani" rel="stylesheet">

<style>
    .product{
        transition: transform .2s;
    }

    .product:hover{
        transform: scale(1.1);
    }

    .font_k{
        font-family:'kavivanar', cursive !important;
    }

    .my-pro{
        border:solid 2px #f44336;
       
    }

    .my-pro:hover{
        filter:brightness(70%);
       
    }
</style>

<section class="container" style="padding-bottom:40px;">
    <br>
    <br>
    <div id="content" style="padding-top:20px;">
        
    </div>
</section>

<?php include("../layouts/footer.php"); ?>

<script src="../js/custom/showcaseproducts.js"></script>