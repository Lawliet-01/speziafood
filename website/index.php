<?php include("../layouts/header.php"); ?>
<link href="https://fonts.googleapis.com/css?family=Dancing+Script|Kavivanar|Rajdhani" rel="stylesheet">
<script src="../js/scrollreveal.js"></script>
<style media="screen">
  
    div a img{
        height: 600px !important; 
        }

   @media only screen 
      and (min-device-width : 320px) 
      and (max-device-width : 480px) {
        div a img{
             height:200px !important;
          }

          .carousel .carousel-slider{
            height:200px;
          }

          .adjust{
            height:600px !important;
          }
        }

    .prodiv{
      -webkit-transition:all .5s ease;
     -moz-transition:all .5s ease;
     -ms-transition:all .5s ease;
     transition: all 0.5s ease;
      cursor:pointer;
    }

    .prodiv:hover{
     filter:brightness(80%);
     margin-top:-10px;
      
    }
    
    .circle-logo {
       /* vertical-align: middle; */
       width: 200px;
    height: 200px;
    border-radius: 100%;
    background: #eee no-repeat center;
    background-size: contain;
    border:solid 4px white;
     }


  </style>
<section id="content" style="">
    
    <div class="carousel carousel-slider">
      <a class="carousel-item" href=""><img src="../image/sliders/slider1.jpg"></a>
      <a class="carousel-item" href=""><img src="../image/sliders/slider2.jpg"></a>
      <a class="carousel-item" href=""><img src="../image/sliders/slider3.jpg"></a>
    </div>

   <div class="row" style="background:#f44336; background-image:url('../image/ba.png'); background-size:cover; background-repeat:no-repeat;  background-position:50% 60%;">
      <div class="adjust row container" style="height:300px;">
        <div class="col l12 s12 m12"> 
            <h2  style="font-weight:300; font-family: 'Dancing Script', cursive;" class="center">Choose The Best, Choose Spezia</h2>
        </div>
        <div class="col l12 s12 m12">
          <p style="font-size: 20px; font-family: 'Kavivanar', cursive !important;" class="lighten-3 foo-4 flow-text">
            At spezia food,we understand how important quality is for our customers.
            We Provide You a wide range of Spices, Herbs, Seasonings and much more that will add the Midas touch to your Food.
         </p>
        </div>
      </div>
    </div>

    <div class="" style="display:flex; justify-content:center;">
      <div class="circle-logo" style="background-image:url('../image/logo.png'); margin-top: -123px;"></div>
    </div>

    <div id="products" class="" style="margin-top:20px; display:flex; justify-content:center;">
      <h2 style="font-weight:300; font-family: 'Dancing Script', cursive;">Our Products</h2>
    </div>
    
    <div  class="container row " style="margin-top:20px; ">
        
        <div class="col l3 s10 offset-s1  offset-m1 m3">
          <div class="card border-pro prodiv foo-2" data-value="0">
            <div class="card-image"> 
            <img style="height:300px;" src="../image/product/oregano.jpg">
                <span class="card-title black-text"></span>
            </div>
            <div class="card-content center" style="background:#f44336;padding-bottom: 9px; padding-top:9px;">
              <Span class="white-text"  style="font-size: 20px; font-family: 'Kavivanar', cursive !important;">Oregano</Span>
            </div>
          </div>
        </div>
        
        
        <div class="col l3 s10 offset-s1 m3 offset-l1 offset-m1">
          <div class="card border-pro prodiv foo-2" data-value="1">
            <div class="card-image"> 
            <img style="height:300px;" src="../image/product/pasta.jpg">
                <span class="card-title black-text"></span>
            </div>
            <div class="card-content center" style="background:#f44336;padding-bottom: 9px; padding-top:9px;">
              <Span class="white-text"  style="font-size: 20px; font-family: 'Kavivanar', cursive !important;">Pizza Seasoning</Span>
            </div>
          </div>
        </div>

        <div class="col l3 s10 offset-s1 m3 offset-l1 offset-m1">
          <div class="card border-pro prodiv foo-2" data-value="2">
            <div class="card-image"> 
            <img style="height:300px;" src="../image/product/chilli.jpg">
                <span class="card-title black-text"></span>
            </div>
            <div class="card-content center" style="background:#f44336;padding-bottom: 9px; padding-top:9px;">
              <Span class="white-text"  style="font-size: 20px; font-family: 'Kavivanar', cursive !important;">Chilli Flakes</Span>
            </div>
          </div>
        </div>
  </div>
    
  <div class="parallax-container">
       <div class="parallax"><img class="responsive-img" src="../image/par3.jpg"></div>
        <div class="container center">
          <h2 class="white-text foo-2" style="font-weight:300; font-family: 'Dancing Script', cursive; margin-top:150px;">Simple & Delicious</h2>
          <h2 class="flow-text white-text foo-4" style="font-weight:400;">Simple ingredients prepared in a simple way - that's the best way to take your everyday cooking to a higher level.</h2>
       </div>
    </div>


    <div class="row center " style=" background:#f44336; display:flex; justify-content:center !important; margin-bottom:-10px;">
      <h2 class="flow-text white-text foo-2" style=" font-family: 'Rajdhani', sans-serif !important; " >
        Are you a stockist looking to sell our delicious spices? 
        <span class="" style="font-weight:bold;"><a href="contactus.php#contact" style="color:#e6ee9c;">Get in touch</a></span>
      </h2>
      
    </div>
   
  
  
  
  </section>

   
 
 <?php include("../layouts/footer.php"); ?>


<script type="text/javascript" src="../js/custom/index.js"></script>
